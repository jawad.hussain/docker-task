FROM ubuntu:bionic

RUN apt-get update && \
	apt-get install -y g++ gcc git cmake make autoconf libtool pkg-config python python3
RUN apt-get install -y libboost-all-dev
WORKDIR /usr/src