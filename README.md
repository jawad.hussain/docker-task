# Docker Task

## Discription:
This file contains a simple hello world c++ program by the name `main.cpp`. In this project we are creating a docker image and running a container to run this code.
  
## How to Run:
1. Clone repo using `git clone https://gitlab.com/jawad.hussain/docker-task.git`.
2. make sure docker is installed.
    ```
    sudo apt-get update

    sudo apt-get install docker-ce docker-ce-cli containerd.io

    ```
   - **NOTE:** It is recommended to look at the official docker page to install docker. https://docs.docker.com/engine/install/ubuntu/#installation-methods
3. Navigate to the cloned folder and run the following commands.  

   - *Setup docker without docker-compose:*  
        ```
        docker build -t image-name .

        docker run --name container-name -it -v /src/path:/usr/src image-name:latest

        ```
        - **image-name** is the name given to the image. You can name it whatever you want but remember to use the same name moving forward.
        - In the end of the 2nd command **latest** is the version number. It is better to give it a number.
        - **container-name** is the name given to the container.
        - You will now be in CLI of the docker file.  
      <br/>


   - *Setup docker with docker-compose:*  
        ```
        docker-compose build

        docker-compose up -d

        docker exec -ti docker-task /bin/bash
        ```
        - **NOTE:** make sure docker-compose is installed.
        
        - **docker-task** is the name given to the container using the .yml file. It can be changed. In case you don't know the name it can be found out by using the command `docker ps` or `docker ps -a` if the container is not running.
         
        - You will now be in the CLI of the docker file.
    - Now you can run the program using the following commands:
        ```

        mkdir build
        
        cd build
        
        cmake ..
        
        make
        
        ./main
        ```
        - To exit CLI anytime use command `exit`

## Closing and removing Container:
- For non-docker-compose approch use the command:
  ```
   docker stop container-name

   docker rm container-name
    ```
- For docker-compose approch use the command:
    ```
    docker-compose down
    ```
